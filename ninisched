#!/usr/bin/env python3

from http.server import BaseHTTPRequestHandler, ThreadingHTTPServer
import socket, ssl, os, json, sys, hashlib
from pathlib import Path
from datetime import datetime, timedelta
from dateutil import tz

PROGNAME = Path(sys.argv[0]).name

try:
    FOLDER = Path(os.environ[f"{PROGNAME.upper()}_FOLDER"]).resolve()
except KeyError:
    try:
        XDG_CONFIG_HOME = Path(os.environ["XDG_CONFIG_HOME"]).resolve()
    except KeyError:
        XDG_CONFIG_HOME = Path(os.environ.get("HOME", ".")).resolve() / ".config"
    FOLDER = XDG_CONFIG_HOME / PROGNAME
if not FOLDER.is_dir():
    raise RuntimeError(f"Error: {FOLDER} is not a folder")

try:
    CONFIG = json.load((FOLDER / "config").open())
except:
    CONFIG = {}

DAYLEN = timedelta(hours=7, minutes=30)
LUNCH = timedelta(minutes=20)
ICALFMT = "%Y%m%dT%H%M%SZ"

def parsetime(date, timestr):
    if len(timestr) <= 2: timestr += "00" # just an hour
    try:
        thetime = datetime.strptime(f"{timestr:0>4}", "%H%M").time()
    except ValueError:
        return None
    result = datetime.combine(date, thetime)
    return result.replace(tzinfo=tz.tzlocal()).astimezone(tz.UTC)

def do_hash(*x):
    return hashlib.sha1(repr(x).encode()).hexdigest()

class Nini():
    def schedule(self):
        ical = ["BEGIN:VCALENDAR",
                f"PRODID: {PROGNAME}",
                "VERSION: 2.0"]

        for sched in sorted(FOLDER.iterdir()):
            try:
                month = datetime.strptime(sched.name, "%Y-%m").date()
                daylist = sched.read_text()
            except:
                continue
            for day, line in enumerate(daylist.splitlines()):
                try:
                    date = month.replace(day=day+1)
                except ValueError:
                    break
                line = (line.split(maxsplit=1) + ["", ""])[:2]
                dtype = line[0].lower()

                if (not dtype) or (dtype == "r"):
                    continue # avoid trying a full parse just to continue
                if dtype == "m":
                    dtype = "0640"
                elif dtype == "j":
                    dtype = "1000"
                elif dtype == "s":
                    dtype = "1350"

                start, *end = dtype.split("-", 1)
                start = parsetime(date, start)
                if start is None: continue

                if end:
                    end = parsetime(date, end[0])
                    if end is None: continue
                else:
                    end = start + DAYLEN
                    if start < parsetime(date, "12") < end:
                        end += LUNCH

                if line[1]:
                    longtype = line[1]
                elif start < parsetime(date, "0730"):
                    longtype = "Matin"
                elif end > parsetime(date, "2030"):
                    longtype = "Soir"
                else:
                    longtype = "Journée"

                uid = do_hash(start, end, longtype)

                ical.extend((
                    "BEGIN:VEVENT",
                    f"UID:{uid}@{PROGNAME}",
                    f"DTSTART:{start:{ICALFMT}}",
                    f"DTSTAMP:{start:{ICALFMT}}",
                    f"DTEND:{end:{ICALFMT}}",
                    f"SUMMARY:{longtype}",
                    "END:VEVENT"))

        ical.extend(("END:VCALENDAR", ""))

        return "\r\n".join(ical)

if __name__ == "__main__":
    sys.stdout.write(Nini().schedule())
